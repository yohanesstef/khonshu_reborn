/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void setupAnalog();
void input_speed();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define M1_PWM_Pin GPIO_PIN_5
#define M1_PWM_GPIO_Port GPIOE
#define PITCH_PWM_Pin GPIO_PIN_6
#define PITCH_PWM_GPIO_Port GPIOE
#define M0_ENCA_Pin GPIO_PIN_0
#define M0_ENCA_GPIO_Port GPIOA
#define M0_ENCB_Pin GPIO_PIN_1
#define M0_ENCB_GPIO_Port GPIOA
#define stick_RX_Pin GPIO_PIN_3
#define stick_RX_GPIO_Port GPIOA
#define M0_DIRA_Pin GPIO_PIN_5
#define M0_DIRA_GPIO_Port GPIOA
#define M0_PWM_Pin GPIO_PIN_7
#define M0_PWM_GPIO_Port GPIOA
#define PITCH_DIRA_Pin GPIO_PIN_2
#define PITCH_DIRA_GPIO_Port GPIOB
#define PITCH_DIRB_Pin GPIO_PIN_7
#define PITCH_DIRB_GPIO_Port GPIOE
#define M1_DIRA_Pin GPIO_PIN_10
#define M1_DIRA_GPIO_Port GPIOE
#define M1_DIRB_Pin GPIO_PIN_13
#define M1_DIRB_GPIO_Port GPIOE
#define YAW_DIRB_Pin GPIO_PIN_12
#define YAW_DIRB_GPIO_Port GPIOB
#define YAW_DIRA_Pin GPIO_PIN_13
#define YAW_DIRA_GPIO_Port GPIOB
#define YAW_PWM_Pin GPIO_PIN_15
#define YAW_PWM_GPIO_Port GPIOB
#define M0_DIRB_Pin GPIO_PIN_8
#define M0_DIRB_GPIO_Port GPIOD
#define proximity_Pin GPIO_PIN_10
#define proximity_GPIO_Port GPIOD
#define usb_tx_Pin GPIO_PIN_9
#define usb_tx_GPIO_Port GPIOA
#define usb_rx_Pin GPIO_PIN_10
#define usb_rx_GPIO_Port GPIOA
#define PITCH_ENCA_Pin GPIO_PIN_15
#define PITCH_ENCA_GPIO_Port GPIOA
#define stick_TX_Pin GPIO_PIN_5
#define stick_TX_GPIO_Port GPIOD
#define PITCH_ENCB_Pin GPIO_PIN_3
#define PITCH_ENCB_GPIO_Port GPIOB
#define M1_ENCA_Pin GPIO_PIN_4
#define M1_ENCA_GPIO_Port GPIOB
#define M1_ENCB_Pin GPIO_PIN_5
#define M1_ENCB_GPIO_Port GPIOB
#define YAW_ENCA_Pin GPIO_PIN_6
#define YAW_ENCA_GPIO_Port GPIOB
#define YAW_ENCB_Pin GPIO_PIN_7
#define YAW_ENCB_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "motor2.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define JSSL 0x1
#define JSL3 0x2
#define JSR3 0x4
#define JSST 0x8
#define JSUP 0x10
#define JSRG 0x20
#define JSDW 0x40
#define JSLF 0x80
#define JSL2 0x100
#define JSR2 0x200
#define JSL1 0x400
#define JSR1 0x800
#define JSTR 0x1000
#define JSCR 0x2000
#define JSXX 0x4000
#define JSSQ 0x8000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//-------extern variable motor.c---------
extern int pwm_yaw, pwm_pitch;
extern int posisi_yaw, posisi_pitch;
extern int16_t input_target_posisi_yaw, input_target_posisi_pitch;
extern int16_t increment_yaw, increment_pitch, increment_roller;

int16_t status_kalibrasi_pitch;
int16_t status_kalibrasi_yaw;
int16_t proximity_status;

int16_t target_yaw, target_pitch, target_roller, ros_yaw, ros_pitch, ros_roller;

union data_union{
	uint8_t kirim[12];
	float data[3];
};

union data_union2{
	uint8_t terima[12];
	float data[3];
};

union data_union buffer_kirim;
union data_union2 buffer_terima;

int mode = 0;


//---ROLLER---
extern int16_t target_speed_roller[2];
extern int16_t speed_roller[2];
int status_increment_roller;

//---JOYSTICK---
uint8_t joystick_value[9];
int8_t joystick_status;
uint16_t joystick_button, buff_btn, prev_buff_btn;
int16_t joystick_analog[3];
float rasio_yaw, rasio_pitch;

//---TES Transmit---
uint8_t tes_kirim[] = "HELLO WORLD \r\n";
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart) {
	if (huart->Instance == USART2) {
		if (joystick_value[0] == 'i' && joystick_value[1] == 't'
				&& joystick_value[2] == 's') {
			joystick_status = 1;
		} else {
			joystick_status = 0;
			HAL_UART_DMAStop(&huart2);
			HAL_UART_Receive_DMA(&huart2, joystick_value, 9);
		}
	}

}

void setupAnalog() {
	if (joystick_status == 1) {
		joystick_button = ~(joystick_value[3] + (joystick_value[4] << 8));
		joystick_analog[0] = (joystick_value[7] - 127);
		joystick_analog[1] = (127 - joystick_value[8]);
		joystick_analog[2] = (joystick_value[5] - 127);
	} else {
		joystick_button = 0;
		joystick_analog[0] = 0;
		joystick_analog[1] = 0;
		joystick_analog[2] = 0;
	}
	rasio_yaw = (float) (abs(joystick_analog[2]) / 128.00);
	rasio_pitch = (float) (abs(joystick_analog[1]) / 128.00);

	if (joystick_analog[2] < 0)
		input_target_posisi_yaw += (int16_t) (increment_yaw * rasio_yaw);
	else if (joystick_analog[2] > 0)
		input_target_posisi_yaw -= (int16_t) (increment_yaw * rasio_yaw);

	if (joystick_analog[1] > 0)
		input_target_posisi_pitch += (int16_t) (increment_pitch * rasio_pitch);
	else if (joystick_analog[1] < 0)
		input_target_posisi_pitch -= (int16_t) (increment_pitch * rasio_pitch);

	switch (joystick_button) {
	case JSXX:
		target_speed_roller[0] = 20;
		target_speed_roller[1] = 10;
		break;

	case JSSQ:
		target_speed_roller[0] = 0;
		target_speed_roller[1] = 0;
		break;
	}
	if (joystick_button == JSTR && status_increment_roller == 0) {
		status_increment_roller = 1;
		target_speed_roller[0] += 4;
		target_speed_roller[1] += 2;
	} else if (joystick_button == JSCR && status_increment_roller == 0) {
		status_increment_roller = 1;
		target_speed_roller[0] -= 4;
		target_speed_roller[1] -= 2;
	} else if (joystick_button == 0) {
		status_increment_roller = 0;
	}
}

void input_speed() {

	if (target_yaw >= 2500)
		target_yaw = 2500;
	else if (target_yaw <= 0)
		target_yaw = 0;
	if (target_pitch >= 2500)
		target_pitch = 2500;
	else if (target_pitch <= 0)
		target_pitch = 0;
	if(target_roller >= 20)target_roller = 20;
	else if(target_roller <= 0)target_roller = 0;
	if(ros_yaw >= 2500)ros_yaw = 2500;
	else if(ros_yaw <= 0)ros_yaw = 0;
	if(ros_pitch >= 2500)ros_pitch = 2500;
	else if(ros_pitch <= 0)ros_pitch = 0;
	if(ros_roller >= 20)ros_roller = 20;
	else if(ros_roller <= 0)ros_roller = 0;

	if (mode == 0){
		if (input_target_posisi_yaw >= target_yaw - increment_yaw && input_target_posisi_yaw <= target_yaw + increment_yaw){
			input_target_posisi_yaw = target_yaw;
		}
		else if (input_target_posisi_yaw < target_yaw) {
			input_target_posisi_yaw += increment_yaw;
		} else if (input_target_posisi_yaw > target_yaw) {
			input_target_posisi_yaw -= increment_yaw;
		}

		if(input_target_posisi_pitch >= target_pitch - increment_pitch && input_target_posisi_pitch <= target_pitch + increment_pitch){
			input_target_posisi_pitch = target_pitch;
		}
		else if (input_target_posisi_pitch < target_pitch) {
			input_target_posisi_pitch += increment_pitch;
		}
		else if (input_target_posisi_pitch > target_pitch) {
			input_target_posisi_pitch -= increment_pitch;
		}
		if (target_speed_roller[0] >= target_roller-increment_roller && target_speed_roller[0] <= target_roller + increment_roller){
			target_speed_roller[0] = target_roller;
		}
		else if (target_speed_roller[0] < target_roller){
			target_speed_roller[0] += increment_roller;
		}
		else if (target_speed_roller[0] > target_roller){
			target_speed_roller[0] -= increment_roller;
		}
	}

	else if (mode == 1){

		if (input_target_posisi_yaw >= ros_yaw - increment_yaw && input_target_posisi_yaw <= ros_yaw + increment_yaw){
			input_target_posisi_yaw = ros_yaw;
		}
		else if (input_target_posisi_yaw < ros_yaw) {
			input_target_posisi_yaw += increment_yaw;
		}
		else if (input_target_posisi_yaw > ros_yaw) {
			input_target_posisi_yaw -= increment_yaw;
		}

		if(input_target_posisi_pitch >= ros_pitch - increment_pitch && input_target_posisi_pitch <= ros_pitch + increment_pitch){
			input_target_posisi_pitch = ros_pitch;
		}
		else if (input_target_posisi_pitch < ros_pitch) {
			input_target_posisi_pitch += increment_pitch;
		}
		else if (input_target_posisi_pitch > ros_pitch) {
			input_target_posisi_pitch -= increment_pitch;
		}

		if (target_speed_roller[0] >= ros_roller-increment_roller && target_speed_roller[0] <= ros_roller+increment_roller){
			target_speed_roller[0] = ros_roller;
		}
		else if (target_speed_roller[0] < ros_roller){
			target_speed_roller[0] += increment_roller;
		}
		else if (target_speed_roller[0] > ros_roller){
			target_speed_roller[0] -= increment_roller;
		}
	}

	target_speed_roller[1] = target_speed_roller[0];
}

void perhitungan_data(){

	buffer_kirim.data[0] = posisi_yaw / 2500.0 * 85.0;
	buffer_kirim.data[1] = posisi_pitch / 2500.0 * 85.0;
	buffer_kirim.data[2] = speed_roller[0];

	ros_yaw = (int16_t)(buffer_terima.data[0] / 85.0 * 2500.0);
	ros_pitch =  (int16_t)(buffer_terima.data[1] / 85.0 * 2500.0);
	ros_roller = buffer_terima.data[2];
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM5_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM9_Init();
  MX_TIM12_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM14_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

	HAL_TIM_Base_Start_IT(&htim7);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_2);

	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_ALL);

	HAL_UART_Receive_DMA(&huart2, joystick_value, 9);

	while(1){//-----kalibrasi yaw
		proximity_status = HAL_GPIO_ReadPin(proximity_GPIO_Port, proximity_Pin);
		pwm_yaw = -50;
		HAL_GPIO_WritePin(YAW_DIRA_GPIO_Port, YAW_DIRA_Pin, pwm_yaw < 0 ? 0 : 1);
		HAL_GPIO_WritePin(YAW_DIRB_GPIO_Port, YAW_DIRB_Pin, pwm_yaw < 0 ? 1 : 0);
		TIM12->CCR2	 = abs(pwm_yaw);

		if (proximity_status == 0){
			pwm_yaw = 0;
			TIM12->CCR2 = 0;
			TIM4->CNT = 0;
			posisi_yaw= 0;
			status_kalibrasi_yaw = 1;
			HAL_Delay(1000);
			break;
		}
	}

	pwm_pitch = -80;
	HAL_GPIO_WritePin(PITCH_DIRA_GPIO_Port, PITCH_DIRA_Pin, pwm_pitch < 0 ? 0 : 1);
	HAL_GPIO_WritePin(PITCH_DIRB_GPIO_Port, PITCH_DIRB_Pin, pwm_pitch < 0 ? 1 : 0);
	TIM9->CCR2 = abs(pwm_pitch);
	HAL_Delay(3000);
	pwm_pitch = 0;
	HAL_GPIO_WritePin(PITCH_DIRA_GPIO_Port, PITCH_DIRA_Pin, pwm_pitch < 0 ? 0 : 1);
	HAL_GPIO_WritePin(PITCH_DIRB_GPIO_Port, PITCH_DIRB_Pin, pwm_pitch < 0 ? 1 : 0);
	TIM9->CCR2 = abs(pwm_pitch);
	HAL_Delay(2000);
	TIM2->CNT = 0;
	posisi_pitch = 0;

	HAL_TIM_Base_Start_IT(&htim6);

//	buffer_kirim.data[0] = 0;
//	buffer_kirim.data[1] = 0;
//	buffer_kirim.data[2] = 0;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		HAL_UART_Transmit(&huart1, &buffer_kirim.kirim, sizeof(buffer_kirim.kirim), 1000);
		HAL_UART_Receive(&huart1, &buffer_terima.terima, 12, 1000);
		perhitungan_data();
//		proximity_status = HAL_GPIO_ReadPin(proximity_GPIO_Port, proximity_Pin);
//		HAL_Delay(1);
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */


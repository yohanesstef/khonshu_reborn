/*
 * motor2.c
 *
 *  Created on: Nov 15, 2022
 *      Author: yohanes
 */

#include "main.h"
#include "motor2.h"
#include "math.h"

//----Roller--------------------------------------------------------------
int16_t speed_roller[2];
int pwm_roller[2];
int16_t increment_roller = 1;
//---pid------------------------------------------------------------------
float kp_roller[2]={10,10};
float ki_roller[2]={0.6,0.6};
float kd_roller[2]={0,0};
float p_roller[2], i_roller[2], d_roller[2];
int16_t sum_pid_roller[2];
int16_t target_speed_roller[2];
int16_t error_roller[2];
int16_t sum_error_roller[2];
int16_t error_roller[2];
int16_t prev_error_roller[2];

//---YAW-------------------------------------------------------------------
int16_t posisi_yaw;
int pwm_yaw;
//---pid----
float kp_yaw = 2.2;
float ki_yaw = 0.25;
float kd_yaw = 0;
float p_yaw, i_yaw, d_yaw;
int16_t sum_pid_yaw;
int16_t target_posisi_yaw;
int16_t error_yaw;
int16_t sum_error_yaw;
int16_t error_yaw;
int16_t prev_error_yaw;
//---increment---
int16_t input_target_posisi_yaw;
int16_t increment_yaw = 15;

//---PITCH---
int16_t posisi_pitch;
int pwm_pitch;
//---pid----
float kp_pitch=2.2;
float ki_pitch=0.1;
float kd_pitch=0;
float p_pitch, i_pitch, d_pitch;
int16_t sum_pid_pitch;
int16_t target_posisi_pitch;
int16_t error_pitch;
int16_t sum_error_pitch;
int16_t error_pitch;
int16_t prev_error_pitch;

//---increment---
int16_t input_target_posisi_pitch;
int16_t increment_pitch = 15;

void kontrol_motor2(){
	//---Roller-------------------------------------------------------------
	speed_roller[0] = -TIM5->CNT;
	speed_roller[1] = -TIM3->CNT;
	TIM5->CNT = 0;
	TIM3->CNT = 0;

	for(int i = 0; i<2 ; i++){
		if(target_speed_roller[i] >= 50)target_speed_roller[i] = 50;
		else if(target_speed_roller[i] <= 0)target_speed_roller[i] = 0;
		error_roller[i] = target_speed_roller[i] - speed_roller[i];
		sum_error_roller[i] += error_roller[i];

		if ((sum_error_roller[i] > 0 && target_speed_roller[i] <= 0) || (sum_error_roller[i] < 0 && target_speed_roller[i] >= 0))sum_error_roller[i] = 0;

		p_roller[i] = error_roller[i] * kp_roller[i];
		i_roller[i] = sum_error_roller[i] * ki_roller[i];
		sum_pid_roller[i] = (int16_t)(p_roller[i] + i_roller[i]);

		if(sum_error_roller[i] >= 2000)sum_error_roller[i] = 2000;
		else if(sum_error_roller[i] <= -2000)sum_error_roller[i] = -2000;

		if(sum_pid_roller[i]>=499)pwm_roller[i] = 499;
		else if(sum_pid_roller[i]<=-499)pwm_roller[i] = -499;
		else pwm_roller[i] = sum_pid_roller[i];
	}

	//---YAW----------------------------------------------------------
	posisi_yaw = (int16_t)(10*TIM4->CNT);
	if(input_target_posisi_yaw >= 2500) input_target_posisi_yaw = 2500;
	else if(input_target_posisi_yaw <= 0) input_target_posisi_yaw = 0;

	error_yaw = input_target_posisi_yaw - posisi_yaw;
	sum_error_yaw += error_yaw;
	if (error_yaw == 0) sum_error_yaw = 0;

	p_yaw = (float)(error_yaw * kp_yaw);
	i_yaw = (float)(sum_error_yaw * ki_yaw);
	d_yaw = (float)((prev_error_yaw - error_yaw) * kd_yaw);
	sum_pid_yaw = (int16_t)(p_yaw + i_yaw + d_yaw);
	prev_error_yaw = error_yaw;

	if(sum_error_yaw >= 2500)sum_error_yaw = 2500;
	else if(sum_error_yaw <= -2500)sum_error_yaw = -2500;

	if(sum_pid_yaw>=200)pwm_yaw = 200;
	else if(sum_pid_yaw<=-200)pwm_yaw = -200;
	else pwm_yaw = sum_pid_yaw;

	//---PITCH------------------------------------------------------
	posisi_pitch = (int16_t)(10*TIM2->CNT);
	if (input_target_posisi_pitch >= 2500)input_target_posisi_pitch = 2500;
	else if (input_target_posisi_pitch <= 0)input_target_posisi_pitch = 0;

	error_pitch = input_target_posisi_pitch - posisi_pitch;
	sum_error_pitch += error_pitch;
	if (error_pitch == 0) sum_error_pitch = 0;

	p_pitch = (float)(error_pitch * kp_pitch);
	i_pitch = (float)(sum_error_pitch * ki_pitch);
	d_pitch = (float)((prev_error_pitch - error_pitch) * kd_pitch);
	sum_pid_pitch = (int16_t)(p_pitch + i_pitch + d_pitch);
	prev_error_pitch = error_pitch;

	if(sum_error_pitch >= 2500)sum_error_pitch = 2500;
	else if(sum_error_pitch <= -2500)sum_error_pitch = 2500;

	if(sum_pid_pitch>=200)pwm_pitch = 200;
	else if(sum_pid_pitch<=-200)pwm_pitch = -200;
	else pwm_pitch = sum_pid_pitch;

	//---Give PWM---
	HAL_GPIO_WritePin(M0_DIRA_GPIO_Port, M0_DIRA_Pin, pwm_roller[0] < 0 ? 1 : 0);
	HAL_GPIO_WritePin(M0_DIRB_GPIO_Port, M0_DIRB_Pin, pwm_roller[0] < 0 ? 0 : 1);
	TIM14->CCR1	 = abs(pwm_roller[0]);

	HAL_GPIO_WritePin(M1_DIRA_GPIO_Port, M1_DIRA_Pin, pwm_roller[1] < 0 ? 1 : 0);
	HAL_GPIO_WritePin(M1_DIRB_GPIO_Port, M1_DIRB_Pin, pwm_roller[1] < 0 ? 0 : 1);
	TIM9->CCR1	 = abs(pwm_roller[1]);

	HAL_GPIO_WritePin(PITCH_DIRA_GPIO_Port, PITCH_DIRA_Pin, pwm_pitch < 0 ? 0 : 1);
	HAL_GPIO_WritePin(PITCH_DIRB_GPIO_Port, PITCH_DIRB_Pin, pwm_pitch < 0 ? 1 : 0);
	TIM9->CCR2	 = abs(pwm_pitch);

	HAL_GPIO_WritePin(YAW_DIRA_GPIO_Port, YAW_DIRA_Pin, pwm_yaw < 0 ? 0 : 1);
	HAL_GPIO_WritePin(YAW_DIRB_GPIO_Port, YAW_DIRB_Pin, pwm_yaw < 0 ? 1 : 0);
	TIM12->CCR2	 = abs(pwm_yaw);
}



/*
 * motor.c
 *
 *  Created on: Sep 8, 2022
 *      Author: yohanes
 */
#include "main.h"
#include "motor.h"
#include "math.h"
//---speed, posisi, pwm----
int16_t speed_motor[2];
int16_t posisi[2];
int pwm[4];
//---PID------
float kpid[4][3]={{10,0.6,0},//roller bawah M0
				 {10,0.6,0},//roller atas M1
				 {0,0,0},//pitch
				 {0,0,0}};//yaw
int sum_error[4];
int16_t error_speed[2];//mo, m1
int16_t error_posisi[2];//pitch, yaw
int16_t target_speed[2];//mo, m1
int16_t target_posisi[2];//pitch, yaw
int16_t prev_error[4];//m0,m1,pitch,yaw
float p[4],j[4],d[4];
int16_t pid[4];
//------------
void kontrol_motor(){
	speed_motor[0] = TIM5->CNT;
	speed_motor[1] = TIM3->CNT;
	posisi[0] = TIM2->CNT;
	posisi[1] = TIM4->CNT;

	TIM5->CNT = 0;
	TIM3->CNT = 0;
//	TIM3->CNT = 0;
//	TIM1->CNT = 0;

	for(int i=0; i<2 ; i++){
		//-------PID Roller-------
		error_speed[i] = target_speed[i] - speed_motor[i];
		sum_error[i] += error_speed[i];
		if((sum_error[i] > 0 && target_speed[i] <= 0) || (sum_error[i] < 0 && target_speed[i] >= 0))sum_error[i]=0;
		p[i] = (float)(error_speed[i] * kpid[i][0]);
		j[i] = (float)(sum_error[i] * kpid[i][1]);
		pid[i] = (int16_t)(p[i]+j[i]);

		if (sum_error[i] >= 2000)sum_error[i] = 1000;
		else if(sum_error[i] <=-2000)sum_error[i] = -1000;

		//-------Limiting PWM-------
		if(pid[i]>=499)pwm[i] = 499;
		else if(pid[i]<=-499)pwm[i] = -499;
		else pwm[i] = pid[i];
	}

	for(int i=0; i<2 ; i++){
		//----PID pitch dan yaw----
		error_posisi[i] = target_posisi[i] - posisi[i];
		sum_error[i+2] += error_posisi[i];
		p[i+2] = (float)(error_posisi[i] * kpid[i+2][0]);
		j[i+2] = (float)(sum_error[i+2] * kpid[i+2][1]);
		d[i+2] = (float)((prev_error[i+2]-error_posisi[i]) *kpid[i+2][2]);
		pid[i+2] = (int16_t)(p[i+2]+j[i+2]+d[i+2]);
		prev_error[i+2] = error_posisi[i];

		if (j[i+2] > 200)j[i+2] = 200;
		else if (j[i+2] < -200)j[i+2] = -200;

		if (sum_error[i+2] >= 2000)sum_error[i+2] = 2000;
		else if(sum_error[i+2] <=-2000)sum_error[i+2] = -2000;

		//---limiting pwm-------
		if(pid[i+2]>=200)pwm[i+2] = 200;
		else if(pid[i+2]<=-200)pwm[i+2] = -200;
		else pwm[i+2] = pid[i+2];
	}


	HAL_GPIO_WritePin(M0_DIRA_GPIO_Port, M0_DIRA_Pin, pwm[0] < 0 ? 0 : 1);
	HAL_GPIO_WritePin(M0_DIRB_GPIO_Port, M0_DIRB_Pin, pwm[0] < 0 ? 1 : 0);

	TIM14->CCR1	 = abs(pwm[0]);

	HAL_GPIO_WritePin(M1_DIRA_GPIO_Port, M1_DIRA_Pin, pwm[1] < 0 ? 1 : 0);
	HAL_GPIO_WritePin(M1_DIRB_GPIO_Port, M1_DIRB_Pin, pwm[1] < 0 ? 0 : 1);
	TIM9->CCR1	 = abs(pwm[1]);

	HAL_GPIO_WritePin(PITCH_DIRA_GPIO_Port, PITCH_DIRA_Pin, pwm[2] < 0 ? 0 : 1);
	HAL_GPIO_WritePin(PITCH_DIRB_GPIO_Port, PITCH_DIRB_Pin, pwm[2] < 0 ? 1 : 0);
	TIM9->CCR2	 = abs(pwm[2]);

	HAL_GPIO_WritePin(YAW_DIRA_GPIO_Port, YAW_DIRA_Pin, pwm[3] < 0 ? 0 : 1);
	HAL_GPIO_WritePin(YAW_DIRB_GPIO_Port, YAW_DIRB_Pin, pwm[3] < 0 ? 1 : 0);
	TIM12->CCR2	 = abs(pwm[3]);
}

